//// Lets the user create a new project.




//// Import shared resources.
import PopupModule from '../class/popup-module.js'
import config from '../core/config.js'

//// Add the route.
config.routes['make-new'] = true

//// Define the ‘MakeNew’ class.
class MakeNew extends PopupModule {

    //// Returns this module’s Vue template-string.
    vueTemplate (innerHTML) { return innerHTML = `
    <div>
      <h1>Make New!</h1>
      <a href="#edit">Go</a>
    </div>
    `}

}

//// Create a singleton instance of this module.
const makeNew = new MakeNew('make-new')
