//// Prompts the user for a commit-message when they click ‘Save’.




//// Import shared resources.
import PopupModule from '../class/popup-module.js'
import config from '../core/config.js'

//// Add the route.
config.routes.commit = true

//// Define the ‘Commit’ class.
class Commit extends PopupModule {

    //// Returns this module’s Vue template-string.
    vueTemplate (innerHTML) { return innerHTML = `
    <div>
      <h1>Commit!</h1>
      <a href="#edit">Commit</a>
    </div>
    `}

}

//// Create a singleton instance of this module.
const commit = new Commit('commit')
