//// Pops up when an error has occurred.




//// Import shared resources.
import PopupModule from '../class/popup-module.js'
import config from '../core/config.js'

//// Add the route.
config.routes.warning = true

//// Define the ‘Warning’ class.
class Warning extends PopupModule {

    //// Returns this module’s Vue template-string.
    vueTemplate (innerHTML) { return innerHTML = `
    <div>
      <h1>Warning!</h1>
      <a href="#edit">OK</a>
    </div>
    `}

}

//// Create a singleton instance of this module.
const warning = new Warning('warning')
