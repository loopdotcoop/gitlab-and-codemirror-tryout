//// The current hash is not recognised.




//// Import shared resources.
import PopupModule from '../class/popup-module.js'
import config from '../core/config.js'

//// Add the route.
config.routes['not-found'] = true

//// Define the ‘NotFound’ class.
class NotFound extends PopupModule {

    //// Returns this module’s Vue template-string.
    vueTemplate (innerHTML) { return innerHTML = `
    <div>
      <h1>Not Found!</h1>
      <a href="#edit">Back</a>
    </div>
    `}

}

//// Create a singleton instance of this module.
const notFound = new NotFound('not-found')
