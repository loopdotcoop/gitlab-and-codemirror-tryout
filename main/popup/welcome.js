//// Appears after the app boots, or when the user closes a project.




//// Import shared resources.
import PopupModule from '../class/popup-module.js'
import config from '../core/config.js'
import state from '../core/state.js'

//// Add the route.
config.routes.welcome = true // also reachable when hash is empty

//// Define the ‘Welcome’ class.
class Welcome extends PopupModule {

    //// Returns this module’s Vue component options.
    vueOptions () { return {
        data: function () { return state.user }
      , template: this.vueTemplate()
    } }

    //// Returns this module’s Vue template-string.
    vueTemplate (innerHTML) { return innerHTML = `
    <div>
      <h1>Welcome {{ username }}</h1>
      <a class="button" href="#make-new">Create a new project</a>
      or
      <a class="button" href="#project-list">Open an existing project</a>
    </div>
    `}

}

//// Create a singleton instance of this module.
const welcome = new Welcome('welcome')
