//// Lets the user clear all in-browser (localStorage) data.




//// DEPENDENCIES

import PopupModule from '../class/popup-module.js'
import config from '../core/config.js'
import hub from '../core/hub.js'
import state from '../core/state.js'




//// ROUTE AND EVENTS

config.routes['purge'] = true

hub.on('purge-completed', evt => window.location.hash = 'welcome')



//// CLASS AND SINGLETON

class Purge extends PopupModule {

    //// Returns this module’s Vue component options.
    vueOptions () { return {
        data: function () { return state }
      , template: this.vueTemplate()
      , methods: {
            attemptPurge: function () {
                hub.fire('purge-requested')
            }
        }
    } }

    //// Returns this module’s Vue template-string.
    vueTemplate (innerHTML) { return innerHTML = `
    <div>
      <h1>Delete offline data?</h1>
      <ul>
        <li v-if="user.username">Username</li>
        <li v-if="user.password">Password</li>
        <li v-if="projectList.list.length">
          {{ projectList.list.length }} Projects</li>
      </ul>
      <button v-on:click="attemptPurge()">Purge Browser</button>
    </div>
    `}

}

//// Create a singleton instance of this module.
const purge = new Purge('purge')
