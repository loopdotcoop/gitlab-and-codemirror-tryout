//// Prompts the user their GitLab username and password.




//// DEPENDENCIES

import PopupModule from '../class/popup-module.js'
import config from '../core/config.js'
import hub from '../core/hub.js'
import state from '../core/state.js'
import util from '../core/util.js'





//// ROUTE AND EVENTS

config.routes['sign-in'] = true

hub.on('gitlab-api-username-not-found', evt => { // no such username
    util.user.falsifyState()
    state.user.usernameNotFound = true
})
hub.on('gitlab-api-invalid-combo', evt => { // bad username/password combo
    util.user.falsifyState()
    state.user.invalidCombo = true
})
hub.on('gitlab-api-network-timeout', evt => { // GitLab too slow
    util.user.falsifyState()
    state.user.networkTimeout = true
})
hub.on('gitlab-api-network-error', evt => { // eg no internet
    util.user.falsifyState()
    state.user.networkError = true
})
hub.on('gitlab-api-handled-error', evt => { // eg expired token
    util.user.falsifyState()
    state.user.gitlabError = evt.payload
})
hub.on('gitlab-api-unexpected-error', evt => { // eg garbled JSON
    util.user.falsifyState()
    state.user.unexpectedError = true
})
hub.on('user-ok', evt => { // general success!
    util.user.falsifyState()
    state.user.isSignedIn = true
    hub.fire('user-updated') // causes the new username/password to be persisted
})
hub.on('user-attempted-sign-in-success', evt => { // specific success!
    window.location.hash = 'welcome'
})




//// CLASS AND SINGLETON

class SignIn extends PopupModule {

    //// Returns this module’s Vue component options.
    vueOptions () { return {
        data: function () { return state.user }
      , template: this.vueTemplate()
      , methods: {
            attemptSignIn: function () {
                if (this.isWaitingForServer) return // ignore multiple clicks

                //// Clear any previous errors.
                util.user.falsifyState()

                //// Basic validation - no need to query the GitLab server.
                this.invalidUsername =
                    ! /^[a-z0-9]{3,32}$/.test(this.username)
                this.invalidPassword =
                    ! /^[_A-Za-z0-9]{3,32}$/.test(this.password)
                if (this.invalidUsername || this.invalidPassword)
                    return this.isSignedIn = false

                //// Signing-in is now pending, until we hear back from GitLab.
                util.user.nullifyState()
                this.isWaitingForServer = true

                //// Try remote validation, using the GitLab API.
                hub.fire('user-attempted-sign-in')
            }
        }
    } }

    //// Returns this module’s Vue template-string.
    vueTemplate (innerHTML) { return innerHTML = `
    <div v-bind:class="{
        invalidUsername
      , invalidPassword
      , usernameNotFound
      , invalidCombo
      , networkTimeout
      , networkError
      , gitlabError
      , unexpectedError
    }">
      <h1>Sign in with your GitLab credentials</h1>
      <input :disabled="isWaitingForServer" class="username" v-model="username" placeholder="Username"></input>
      <input :disabled="isWaitingForServer" class="password" v-model="password" placeholder="password" type="password"></input>
      <button :disabled="isWaitingForServer" v-on:click="attemptSignIn()">Go</button>
      <p v-if="invalidUsername" class="invalid">Invalid username!</p>
      <p v-if="invalidPassword" class="invalid">Invalid password!</p>
      <p v-if="usernameNotFound" class="invalid">GitLab does not recognise that username!</p>
      <p v-if="invalidCombo" class="invalid">Password is incorrect!</p>
      <p v-if="networkTimeout" class="invalid">${config.gitlabApi.domain} did not respond within ${config.gitlabApi.timeout} milliseconds!</p>
      <p v-if="networkError" class="invalid">Cannot contact ${config.gitlabApi.domain}.<br>Check your connection!</p>
      <p v-if="gitlabError" class="invalid">${config.gitlabApi.domain} error:<br>{{ gitlabError.error_description }}</p>
      <p v-if="unexpectedError" class="invalid">Unexpected error!</p>
    </div>
    `}

}

//// Create a singleton instance of this module.
const signIn = new SignIn('sign-in')
