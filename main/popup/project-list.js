//// Lists existing projects, and lets the user open them.




//// DEPENDENCIES

import PopupModule from '../class/popup-module.js'
import config from '../core/config.js'
import hub from '../core/hub.js'
import state from '../core/state.js'
import util from '../core/util.js'




//// UTILITY

util.projectList = {

    //// Inserts any pre-existing projects with just the ‘null’ project and a
    //// sample project. Also points `current` at the null project.
    resetState () {
        state.projectList = state.projectList || {}
        state.projectList.list = [
            {
                id: 0
              , name: "null"
              , description: "The ‘current project’ when no project is selected"
            }
          , {
                avatar_url: null
              , created_at: "2018-01-01T00:00:00.001Z"
              , default_branch: "master"
              , description: "An example GitLab project, used as a sandbox"
              , forks_count: 0
              , http_url_to_repo: "https://gitlab.com/dummyusername/an-example-gitlab-project.git"
              , id: 9999999
              , last_activity_at: "2018-01-01T00:00:00:.002Z"
              , name: "A Dummy GitLab Project"
              , name_with_namespace: "Dummy Username / A Dummy GitLab Project"
              , path: "an-example-gitlab-project"
              , path_with_namespace: "dummyusername/an-example-gitlab-project"
              , readme_url: "https://gitlab.com/dummyusername/an-example-gitlab-project/blob/master/README.md"
              , ssh_url_to_repo: "git@gitlab.com:dummyusername/an-example-gitlab-project.git"
              , star_count: 0
              , tag_list: []
              , web_url: "https://gitlab.com/dummyusername/an-example-gitlab-project"
            }
        ]
        state.projectList.current = state.projectList.list[0]
    }
}




//// ROUTE, STATE AND EVENTS

//// Add the route. @TODO new style!
config.routes['project-list'] = true
hub.on('route-changed-to-project-list', evt => state.popups.current = 'project-list')
// hub.on('route-changed:project-list'
//   , hub.fire('state-needs-update', ['popups.current', 'project-list']))
// hub.on('route-changed:project-list' // triggered by router.js
//   , hub.fire('set-state', 'popup.current', 'project-list')) // heard by state.js

//// Create the `list` array and `current` pointer.
util.projectList.resetState()

//// When a new list of projects is fetched, merge it with the existing list.
hub.on('project-list-fetched', evt => {
    for (const newProj of evt.payload) {
        const existingProjIndex =
            state.projectList.list.findIndex( proj => proj.id === newProj.id )
        if (0 > existingProjIndex)
            state.projectList.list.push(newProj)
        else { // replace existing project meta with new meta
            state.projectList.list.splice(existingProjIndex, 1, newProj)
        }
    }
    hub.fire('project-list-updated') // persist the newly merged project-list
})




//// CLASS AND SINGLETON

class ProjectList extends PopupModule {

    //// Returns this module’s Vue component options.
    vueOptions () { return {
        data: function () { return {
            user: state.user
          , projectList: state.projectList
        } }
      , template: this.vueTemplate()
      , methods: {
            onRefresh: function () {
                console.log(1);
                hub.fire('project-list-attempted-refresh')
            }
        }
    } }

    //// Returns this module’s Vue template-string.
    vueTemplate (innerHTML) { return innerHTML = `
    <div>
      <h1>Project List</h1>
      <p>
        <a v-if="! user.isSignedIn" href="#sign-in">Sign In</a>
        <button v-if="user.isSignedIn" v-on:click="onRefresh()">Refresh</button>
        <p v-if="user.usernameNotFound" class="invalid">GitLab does not recognise that username!</p>
        <p v-if="user.invalidCombo" class="invalid">Password is incorrect!</p>
        <p v-if="user.networkTimeout" class="invalid">${config.gitlabApi.domain} did not respond within ${config.gitlabApi.timeout} milliseconds!</p>
        <p v-if="user.networkError" class="invalid">Cannot contact ${config.gitlabApi.domain}.<br>Check your connection!</p>
        <p v-if="user.gitlabError" class="invalid">${config.gitlabApi.domain} error:<br>{{ user.gitlabError.error_description }}</p>
        <p v-if="user.unexpectedError" class="invalid">Unexpected error!</p>
      </p>
      <ul>
        <li v-for="project in projectList.list" v-if="0 !== project.id">
          <a v-bind:href="'#edit/'+project.id">{{ project.name }}</a>
        </li>
      </ul>
    </div>
    `}

}

//// Create a singleton instance of this module.
const projectList = new ProjectList('project-list')
