//// Initialises the app.




//// LOAD MODULES

//// Core shared resources.
import './core/polyfill.js'
import config from './core/config.js'
import hub from './core/hub.js'
import state from './core/state.js'
import router from './core/router.js'
import persist from './core/persist.js'
import './core/gitlab-api.js'

//// Prepare for the moment when all modules are ready.
hub.on('all-modules-ready', boot)

//// Grid modules (in order of display on portrait mobile device).
expect('header');          import './grid/header.js'
expect('user');            import './grid/user.js'
expect('meta');            import './grid/meta.js'
expect('standard-assets'); import './grid/standard-assets.js'
expect('custom-assets');   import './grid/custom-assets.js'
expect('code-editor');     import './grid/code-editor.js'
expect('config-gui');      import './grid/config-gui.js'
expect('preview');         import './grid/preview.js'
expect('menu-and-cli');    import './grid/menu-and-cli.js'
expect('footer');          import './grid/footer.js'
// expect('grid');            import './grid/grid.js'

//// Popup modules (order is not important).
expect('commit');          import './popup/commit.js'
expect('make-new');        import './popup/make-new.js'
expect('not-found');       import './popup/not-found.js'
expect('project-list');    import './popup/project-list.js'
expect('purge');           import './popup/purge.js'
expect('sign-in');         import './popup/sign-in.js'
expect('warning');         import './popup/warning.js'
expect('welcome');         import './popup/welcome.js'

//// Called before loading each module, so that we can track app readiness.
function expect (id) {
    state.isReady[id] = false // changes to `true` when the module is ready
    hub.on(id+'-ready', onReady)
}




//// LAUNCH THE APP

//// Each module listens for the 'get-ready' event, before initialising.
hub.fire('get-ready')




//// EVENT HANDLERS

//// Checks whether the app is ready to boot, each time a module says its ready.
function onReady (evt) {
    const
        id = evt.evtName.slice(0, -6) // 6 is the trailing '-ready' length
      , total = Object.keys(state.isReady).length
    state.isReady[id] = true //@TODO check for irregularities
    for (const n in state.isReady)
        if (! state.isReady[n]) return // not all ready
    if (config.devLogLevel) console.log(`All ${total} modules are ready`)
    hub.fire('all-modules-ready')
}

//// Boots the app when all modules are ready.
function boot () {

    //// Set up Vue.
    state.vue = new Vue({
        el: '#main'
      , data: {}
      , computed: {}
    })

    //// Load persisted client-side data into app state.
    persist.loadIntoState()

    //// Convert the URL’s initial hash to a route.
    router.onHashChange()

    if (config.devLogLevel) console.log('booted!')
}
