//// Base class for the app’s non-core modules, eg main/grid/header.js




//// Import shared resources.
import hub from '../core/hub.js'

//// Define the ‘BaseModule’ class.
export default class BaseModule {

    constructor (id) {

        //// Record the non-core module’s id.
        this.id = id

        //// Create a wrapper element for it - <DIV> by default.
        this.$wrap = document.createElement( this.wrapTagName )
        this.$wrap.id = this.id
        if (this.wrapClassName) this.$wrap.className = this.wrapClassName
        this.$wrapParent.appendChild(this.$wrap)

        //// Define this module’s Vue component, eg <v-header>.
        Vue.component( 'v-'+this.id, this.vueOptions.apply(this) )

        //// When boot.js fires 'get-ready', create the Vue instance...
        hub.on('get-ready', evt => {
// setTimeout( () => {
// console.log(id, this.$wrap, 111);
            this.$wrap.appendChild( document.createElement('v-'+this.id) )

            //// ...and signal that this module is ready.
            hub.fire(this.id+'-ready')

            //// @TODO Also fire an event when the Vue instance and its child-elements have mounted
// },1000)
        })


    }

    //// The ‘tagName’ of this module’s wrapper element.
    get wrapTagName () {
        return 'DIV' // the main/grid/header.js module uses 'HEADER'
    }

    //// The initial ‘class’ attribute of this module’s wrapper element.
    get wrapClassName () {
        return false // empty by default
    }

    //// The HTML element which contains all BaseModule wraps.
    get $wrapParent () {
        return document.body // never called - BaseModule is not used directly
    }

    //// Returns this module’s Vue component options.
    vueOptions () { return {
        data: function () {
          return {
            // count: 0
          }
        }
      , template: this.vueTemplate()
    } }

    //// Returns this module’s Vue template-string. The `innerHTML` persuades
    //// Atom editor to HTML-highlight the string.
    vueTemplate (innerHTML) { return innerHTML = `
<!--<button v-on:click="count++">You clicked me {{ count }} times...</button>-->
    `}

}
