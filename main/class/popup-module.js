//// Base class for the app’s popup modules, eg main/popup/welcome.js




//// Import shared resources.
import BaseModule from './base-module.js'

//// Define the ‘PopupModule’ class.
export default class PopupModule extends BaseModule {

    //// The HTML element which contains all PopupModule wraps.
    get $wrapParent () {
        return document.querySelector('#popup')
    }

}
