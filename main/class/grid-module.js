//// Base class for the app’s grid modules, eg main/popup/meta.js




//// Import shared resources.
import BaseModule from './base-module.js'

//// Define the ‘PopupModule’ class.
export default class GridModule extends BaseModule {

    //// The HTML element which contains all GridModule wraps.
    get $wrapParent () {
        return document.querySelector('#grid')
    }

    //// Most of the grid-modules are <NAV>.
    get wrapTagName () { return 'NAV' }

}
