//// Converts the current URL’s hash into a view with arguments.




//// DEPENDENCIES

import config from './config.js'
import hub from './hub.js'
import state from './state.js'




//// CONFIG AND STATE

//// Non-core modules can add their own routes in here.
config.routes = {
    edit: true // the normal project-editing view
  , preview: true // maximise the project preview
}

//// Add reactive state data.
state.router = {
    current: null // a string, which must be a key in config.routes
  , history: [] // previous values of state.router.current
}




//// CLASS AND SINGLETON

class Router {
    constructor () {
        window.addEventListener('hashchange', this.onHashChange, false)
    }

    onHashChange () {

        //// Get the new hash, and try to find its route.
        let parts = window.location.hash.slice(1).split('/')
        let view = parts[0]
        if (! view) view = 'welcome'
        let route = config.routes[view]
        if (! route) view = 'not-found'

        //// Hide all popups.
        const $$popups = Array.from( document.querySelectorAll('#popup >div') )
        $$popups.map( $popup => $popup.classList.remove('show') )
        document.body.classList.remove('show-popup')

        //// Show the project-edit view. The event payload is the (numeric) id.
        if ('edit' === view)
            return hub.fire('routed-to-edit', +parts[1])

        //// Show the project-preview view.
        if ('preview' === view)
            return hub.fire('routed-to-preview', +parts[1])

        //// Show the correct popup.
        document.body.classList.add('show-popup')
        document.querySelector('#'+view).classList.add('show')

    }
}

//// Make a single instance of the Router class available to all modules.
export default new Router()
