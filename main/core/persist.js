//// Provides client-side persistance of app state.




//// DEPENDENCIES

import state from './state.js'
import hub from './hub.js'
import util from './util.js'




//// CLASS AND SINGLETON

class Persist {

    constructor () {
        hub.on('purge-requested', this.onPurge)
        hub.on('user-updated', this.onUpdate)
        hub.on('project-list-updated', this.onUpdate)
    }

    //// Deletes everything in localStorage. Triggered by main/popups/purge.js.
    onPurge (evt) {
        window.localStorage.removeItem('user')
        window.localStorage.removeItem('project-list')
        util.projectList.resetState()
        util.user.logOutState()
        hub.fire('user-updated') // persist empty-string usrnm/password
        hub.fire('purge-completed')
    }

    //// Records current state into localStorage. Triggered whenever persisted
    //// state-data changes.
    onUpdate (evt) {
        if ('user-updated' === evt.evtName) {
            console.log(evt.evtName, state.user, JSON.stringify(state.user));
            window.localStorage.setItem( 'user', JSON.stringify(state.user) )}
        if ('project-list-updated' === evt.evtName)
            window.localStorage.setItem( 'project-list', JSON.stringify(state.projectList.list) )
    }

    //// Loads state from localStorage into the app’s `state` object. Called by
    //// main/boot.js as the app starts up.
    loadIntoState () {
        const userJSON = window.localStorage.getItem('user')
        console.log('userJSON', userJSON);
        if (userJSON) {
            const user = JSON.parse(userJSON)
            Object.keys(state.user).map( key => state.user[key] = user[key])
        }
        const projectListJSON = window.localStorage.getItem('project-list')
        if (projectListJSON) {
            state.projectList.list = JSON.parse(projectListJSON)
        }
    }

}

//// Make a single instance of the Persist class.
export default new Persist()
