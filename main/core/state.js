//// Shared app state.

export default {
    isReady: {} // used by boot.js to track each module as they initialise
  , vue: null // the global Vue instance, created by main/boot.js:boot()
}
