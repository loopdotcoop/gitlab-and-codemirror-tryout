//// A collection of handy polyfills.




////
window.AbortController = window.AbortController || (class AbortController {
    abort () {}
    get signal () { return { aborted:false, onabort:null, ispoly:1 } }
})
