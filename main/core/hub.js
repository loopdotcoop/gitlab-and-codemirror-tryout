//// Recieves and broadcasts events. Helps keep things loosely coupled.




//// Define the 'Hub' class.
class Hub {
    constructor () {
        this.handlers = {} // keys are event names, values are arrays of handlers
    }

    on (evtName, handler) {
        this.handlers[evtName] = this.handlers[evtName] || [] // create if needed
        this.handlers[evtName].push(handler)
    }

    fire (evtName, ...payload) {
        if (1 === payload.length) payload = payload[0]
        const handler = this.handlers[evtName]
        if (! handler) return // no such handler
        handler.map( fn => fn({ evtName, payload }) )
    }
}

//// Make a single instance of the Hub class available to all modules.
export default new Hub()
