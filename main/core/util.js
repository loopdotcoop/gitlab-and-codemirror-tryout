//// Various handy utilities.

export default {

    //// The Fetch API, but rejects if the request takes too long.
    //// from https://gist.github.com/davej/728b20518632d97eef1e5a13bf0d05c7
    fetchWithTimeout: function (url, options, timeout=7000) { // milliseconds
        return Promise.race([
            fetch(url, options)
          , new Promise( (_, reject) =>
                setTimeout( () => reject( new Error('timeout') ), timeout )
            )
        ])
    }

}
