//// Communication with GitLab.




//// DEPENDENCIES

import config from './config.js'
import hub from './hub.js'
import state from './state.js'
import util from './util.js'




//// INITIALISE CONFIG

//// Non-core modules can add their own routes in here.
config.gitlabApi = {
    domain: 'https://gitlab.com/api/v4' // no trailing slash
  , timeout: 5000 // milliseconds
}




//// CLASS AND SINGLETON

class GitlabApi {

    constructor () {
        hub.on('user-attempted-sign-in', this.fetchProjectList)
        hub.on('project-list-attempted-refresh', this.fetchProjectList)
        hub.on('routed-to-edit', this.onRoutedToEditOrPreview)
        hub.on('routed-to-preview', this.onRoutedToEditOrPreview)
    }

    //// Fetches the specified project from GitLab (unless it’s already loaded).
    onRoutedToEditOrPreview (evt) {

        //// If the specified project is already currently loaded, do nothing.
        if (state.projectList.current && evt.payload === state.projectList.current.id)
            return

        //// Find the project in the project-list, based on its ID.
        let project = false
        for (const proj of state.projectList.list)
            if (proj.id === evt.payload) { project = proj; break }

        //// If the specified project is not listed, reject it.
        if (! project) {
            if (config.devLogLevel) console.error('No project ID', evt.payload)
            return
        }

        //// Point state.projectList.current at the project from the list.
        //// main/grid/meta.js will immediately update. The other grid modules
        //// will be emptied, unless the specified project has content.
        state.projectList.current = project

        //// If the specified project does not contain any content (eg it was
        //// fetched as meta-only during user sign-in), fetch the content.
        //@TODO

    }

    //// Triggered when the project-list is refreshed, and also when the user
    //// attempts to sign into GitLab.
    fetchProjectList (evt) {
        const
            url =
                config.gitlabApi.domain
              + '/users/'
              + state.user.username
              + '/projects?private_token='
              + state.user.password
              + '&simple=true'
          , controller = new AbortController() //@TODO test x-browser
          , signal = controller.signal //@TODO test x-browser
          , opt = { signal }

        util.fetchWithTimeout(url, opt, config.gitlabApi.timeout)
           .then(resRaw => {
                return resRaw.json()
            })
           .then(res => {
                if (! res)
                    throw Error('no-response')
                if ('404 User Not Found' === res.message) // no such username
                    return hub.fire('gitlab-api-username-not-found')
                if ('401 Unauthorized' === res.message) // bad usrnm/pwd combo
                    return hub.fire('gitlab-api-invalid-combo')
                if (res.error) // eg expired token
                    return hub.fire('gitlab-api-handled-error', res) // payload
                if (! Array.isArray(res) )
                    throw Error('response-not-array')
                hub.fire('project-list-fetched', res) // note the payload here
                hub.fire('user-ok') // general success!
                hub.fire(evt.evtName+'-success') // specific success!
            })
           .catch(error => {
                controller.abort() // close the request, if still open @TODO also, add a ‘Cancel’ button
                if (config.devLogLevel) console.error('Error:', error)
                if ('no-response' === error.message) //@TODO remove if impossible
                    return hub.fire('gitlab-api-unexpected-error')
                if ('response-not-array' === error.message) // eg garbled JSON
                    return hub.fire('gitlab-api-unexpected-error')
                if ('timeout' === error.message) // GitLab too slow
                    return hub.fire('gitlab-api-network-timeout')
                hub.fire('gitlab-api-network-error') // eg no internet
            })
    }

}

//// Create a singleton instance of this module.
const gitlabApi = new GitlabApi()
