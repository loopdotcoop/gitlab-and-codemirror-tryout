//// Sliders, radio-buttons etc, which represent a project’s config.js file.




//// Import shared resources.
import GridModule from '../class/grid-module.js'
import state from '../core/state.js'

//// Add reactive state data.
state.configGui = {
    inputs: 'Gui inputs here'
}

//// Define the ‘ConfigGui’ class.
class ConfigGui extends GridModule {

    //// Returns this module’s Vue component options.
    vueOptions () { return {
        data: function () { return state.configGui }
      , template: this.vueTemplate()
    } }

    //// Returns this module’s Vue template-string.
    vueTemplate (innerHTML) { return innerHTML = `
    <div>
      <h1>Config Gui</h1>
      <h4>{{ inputs }}</h4>
    </div>
    `}

    //// The initial ‘class’ attribute of this module’s wrapper element.
    get wrapClassName () { return 'bottom left1' }

}

//// Create a singleton instance of this module.
const configGui = new ConfigGui('config-gui')
