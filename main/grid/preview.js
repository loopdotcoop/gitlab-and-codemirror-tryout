//// Demo of the current project. Usually a small panel but can fill the window.




//// Import shared resources.
import GridModule from '../class/grid-module.js'
import state from '../core/state.js'

//// Add reactive state data.
state.preview = {
    html: 'HTML for preview'
}

//// Define the ‘Preview’ class.
class Preview extends GridModule {

    //// Returns this module’s Vue component options.
    vueOptions () { return {
        data: function () { return state.preview }
      , template: this.vueTemplate()
    } }

    //// Returns this module’s Vue template-string.
    vueTemplate (innerHTML) { return innerHTML = `
    <div>
      <h1>Preview</h1>
      <h4>{{ html }}</h4>
    </div>
    `}

    //// The ‘tagName’ of this module’s wrapper element.
    get wrapTagName () { return 'DIV' }

    //// The initial ‘class’ attribute of this module’s wrapper element.
    get wrapClassName () { return 'bottom left2' }

}

//// Create a singleton instance of this module.
const preview = new Preview('preview')
