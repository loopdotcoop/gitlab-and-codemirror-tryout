//// Sits at the bottom of the app.




//// Import shared resources.
import GridModule from '../class/grid-module.js'

//// Define the ‘Footer’ class.
class Footer extends GridModule {

    //// Returns this module’s Vue template-string.
    vueTemplate (innerHTML) { return innerHTML = `
    <p>This is the footer</p>
    `}

    //// The ‘tagName’ of this module’s wrapper element.
    get wrapTagName () { return 'FOOTER' }

}

//// Create a singleton instance of this module.
const footer = new Footer('footer')
