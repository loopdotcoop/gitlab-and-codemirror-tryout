//// The current user’s GitLab username, password, etc.




//// DEPENDENCIES

import GridModule from '../class/grid-module.js'
import hub from '../core/hub.js'
import state from '../core/state.js'
import util from '../core/util.js'




//// UTILITY

util.user = {

    //// Sets all user-state flags to `false`. Used by main/popup/sign-in.js.
    falsifyState () {
        state.user.isWaitingForServer = false
        state.user.invalidUsername = false
        state.user.invalidPassword = false
        state.user.usernameNotFound = false
        state.user.invalidCombo = false
        state.user.networkTimeout = false
        state.user.networkError = false
        state.user.gitlabError = false
        state.user.unexpectedError = false
    }

    //// Sets all user-state flags to `null`, meaning they’re currently in an
    //// undecided state.
 ,  nullifyState () {
        state.user.isWaitingForServer = null
        state.user.invalidUsername = null
        state.user.invalidPassword = null
        state.user.usernameNotFound = null
        state.user.invalidCombo = null
        state.user.networkTimeout = null
        state.user.networkError = null
        state.user.gitlabError = null
        state.user.unexpectedError = null
    }

    //// As above, but also sets the username and password to empty strings,
    //// and sets the isSignedIn flag to false. Used when the app first loads
    //// (before persist data is read), and also when the user logs out.
  , logOutState () {
        state.user.username = ''
        state.user.password = ''
        state.user.isSignedIn = false
        util.user.nullifyState()
    }
}




//// STATE

state.user = {}
util.user.logOutState() // but don’t fire 'user-updated'




//// CLASS AND SINGLETON

class User extends GridModule {

    //// Returns this module’s Vue component options.
    vueOptions () { return {
        data: function () { return state.user }
      , template: this.vueTemplate()
      , methods: {
            onLogOut: function () {
                util.user.logOutState()
                hub.fire('user-updated') // persist empty-string usrnm/password
            }
        }
    } }

    //// Returns this module’s Vue template-string.
    vueTemplate (innerHTML) { return innerHTML = `
    <div>
      <p v-if="isSignedIn">
        <b>{{ username }}</b>
        <button v-on:click="onLogOut()">Log Out</button>
      </p>
      <p v-else><a class="button" href="#sign-in">Sign In</a></p>
    </div>
    `}

}

//// Create a singleton instance of this module.
const user = new User('user')
