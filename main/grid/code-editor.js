//// The main sourcecode editing panel, based on CodeMirror.




//// Import shared resources.
import GridModule from '../class/grid-module.js'
import state from '../core/state.js'

//// Add reactive state data.
state.codeEditor = {
    code: 'Code in here'
}

//// Define the ‘CodeEditor’ class.
class CodeEditor extends GridModule {

    //// Returns this module’s Vue component options.
    vueOptions () { return {
        data: function () { return state.codeEditor }
      , template: this.vueTemplate()
    } }

    //// Returns this module’s Vue template-string.
    vueTemplate (innerHTML) { return innerHTML = `
    <div>
      <h1>Code Editor</h1>
      <pre>{{ code }}</pre>
    </div>
    `}

    //// The initial ‘class’ attribute of this module’s wrapper element.
    get wrapClassName () { return 'middle left' }

}

//// Create a singleton instance of this module.
const codeEditor = new CodeEditor('code-editor')
