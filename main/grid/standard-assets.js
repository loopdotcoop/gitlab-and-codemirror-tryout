//// The current project’s standard files, eg README.md and style.css.




//// Import shared resources.
import GridModule from '../class/grid-module.js'
import state from '../core/state.js'

//// Add reactive state data.
state.standardAssets = {
    assets: 123
}

//// Define the ‘StandardAssets’ class.
class StandardAssets extends GridModule {

    //// Returns this module’s Vue component options.
    vueOptions () { return {
        data: function () { return state.standardAssets }
      , template: this.vueTemplate()
    } }

    //// Returns this module’s Vue template-string.
    vueTemplate (innerHTML) { return innerHTML = `
    <div>
      <h1>Standard Assets</h1>
      <h4>{{ assets }}</h4>
    </div>
    `}

    //// The initial ‘class’ attribute of this module’s wrapper element.
    get wrapClassName () { return 'top right' }

}

//// Create a singleton instance of this module.
const standardAssets = new StandardAssets('standard-assets')
