//// Sits at the top of the app.




//// Import shared resources.
import GridModule from '../class/grid-module.js'

//// Define the ‘Header’ class.
class Header extends GridModule {

    //// Returns this module’s Vue template-string.
    vueTemplate (innerHTML) { return innerHTML = `
    <p>GitLab and CodeMirror Tryout</p>
    `}

    //// The ‘tagName’ of this module’s wrapper element.
    get wrapTagName () { return 'HEADER' }

}

//// Create a singleton instance of this module.
const header = new Header('header')
