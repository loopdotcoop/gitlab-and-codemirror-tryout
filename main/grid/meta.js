//// The current project’s title, version, etc.




//// Import shared resources.
import GridModule from '../class/grid-module.js'
import state from '../core/state.js'

// //// Add reactive state data.
// state.meta = {
//     title: 'Untitled'
//   , version: '0.0.0'
//   , url: 'URL not set'
// }
//
//// Define the ‘Meta’ class.
class Meta extends GridModule {

    //// Returns this module’s Vue component options.
    vueOptions () { return {
        data: function () { return state.projectList }
      , template: this.vueTemplate()
    } }

    //// Returns this module’s Vue template-string.
    vueTemplate (innerHTML) { return innerHTML = `
    <div v-if="0 !== current.id">
      <h1>{{ current.name }}</h1>
      <h4>{{ current.description }}</h4>
      <p>{{ current.id }}</p>
    </div>
    `}

    //// The initial ‘class’ attribute of this module’s wrapper element.
    get wrapClassName () { return 'top left' }

}

//// Create a singleton instance of this module.
const meta = new Meta('meta')
