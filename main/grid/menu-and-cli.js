//// The menu is a set of clickable icons. The CLI replaces it with a simple
//// REPL command-line interface, which can simulate all UI interactions.




//// Import shared resources.
import GridModule from '../class/grid-module.js'
import state from '../core/state.js'

//// Add reactive state data.
state.menuAndCli = {
    buttons: [
        { title:'Projects', hash:'project-list' }
      , { title:'Save', hash:'commit' }
      , { title:'Purge', hash:'purge' }
    ]
}

//// Define the ‘MenuAndCli’ class.
class MenuAndCli extends GridModule {

    //// Returns this module’s Vue component options.
    vueOptions () { return {
        data: function () { return state.menuAndCli }
      , template: this.vueTemplate()
    } }

    //// Returns this module’s Vue template-string.
    vueTemplate (innerHTML) { return innerHTML = `
    <div>
      <h1>Menu and CLI</h1>
      <p>
        <a v-for="button in buttons" v-bind:href="'#'+button.hash">
          {{ button.title }}
        </a>
      </p>
    </div>
    `}

    //// The initial ‘class’ attribute of this module’s wrapper element.
    get wrapClassName () { return 'bottom right' }

}

//// Create a singleton instance of this module.
const menuAndCli = new MenuAndCli('menu-and-cli')
