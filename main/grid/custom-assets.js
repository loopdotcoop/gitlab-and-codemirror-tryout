//// A scrolling list of nonstandard assets, eg JPEGs or MP3s.




//// Import shared resources.
import GridModule from '../class/grid-module.js'
import state from '../core/state.js'

//// Add reactive state data.
state.customAssets = {
    assets: 456
}

//// Define the ‘CustomAssets’ class.
class CustomAssets extends GridModule {

    //// Returns this module’s Vue component options.
    vueOptions () { return {
        data: function () { return state.customAssets }
      , template: this.vueTemplate()
    } }

    //// Returns this module’s Vue template-string.
    vueTemplate (innerHTML) { return innerHTML = `
    <div>
      <h1>Custom Assets</h1>
      <h4>{{ assets }}</h4>
    </div>
    `}

    //// The initial ‘class’ attribute of this module’s wrapper element.
    get wrapClassName () { return 'middle right' }

}

//// Create a singleton instance of this module.
const customAssets = new CustomAssets('custom-assets')
