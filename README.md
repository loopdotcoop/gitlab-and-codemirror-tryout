# GitLab and CodeMirror Tryout

#### A CodeMirror frontend, pushing saves to GitLab — just a quick proof-of-tech for now

[Visit the online demo](https://loopdotcoop.gitlab.io/gitlab-and-codemirror-tryout/)
